<?php

namespace AppBundle\Controller\Main;

use AppBundle\Entity\Book;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends Controller
{

    /**
     * Главная страница с формами регистрации агентства и фрилансера
     *
     * @Route("/", name="homepage")
     * @Template(":Main:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        return [];
    }
    /**
     *
     * @Route("/createbook", name="create")
     */
    public function createAction()
    {
        //phpinfo();
        $book = new Book();
        $book->setName('Science');
        $book->setPrice(19.99);
        $book->setDescription('Ergonomic and stylish!');

        $em = $this->getDoctrine()->getManager();

        // tells Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($book);

        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return new Response('Saved new book with id '.$book->getId());
    }
    /**
     *
     * @Route("/showbook/{bookId}", name="show")
     * @Template(":Main:show.html.twig")
     */
    public function showAction($bookId)
    {
        $book = $this->getDoctrine()
            ->getRepository('AppBundle:Book')
            ->find($bookId);

        if (!$book) {
            throw $this->createNotFoundException(
                'No book found for id '.$bookId
            );
        }

        // ... do something, like pass the $product object into a template
        return [
            'book' => $book
        ];
    }

    /**
     *
     * @Route("/updatebook/{bookId}", name="update")
     * @Template(":Main:show.html.twig")
     */
    public function updateAction($bookId)
    {
        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository('AppBundle:Book')->find($bookId);

        if (!$book) {
            throw $this->createNotFoundException(
                'No book found for id '.$bookId
            );
        }

        $book->setName('New book name!');
        $em->flush();

        return [
            'book' => $book
        ];
    }

    /**
     *
     * @Route("/deletebook/{bookId}", name="delete")
     *
     */
    public function deleteAction($bookId)
    {
        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository('AppBundle:Book')->find($bookId);

        if (!$book) {
            throw $this->createNotFoundException(
                'No book found for id '.$bookId
            );
        }

        $em->remove($book);
        $em->flush();

        return new Response('Deleted book with id - '.$bookId);
    }

    public function setLocaleAction()
    {
        return $this->redirect($this->generateUrl('homepage'));
    }

    public function redirectToMainMirrorAction(Request $request)
    {
        //убираем только первую встреченную строку 'www.' в урле
        $url = preg_replace('/www\./', '', $request->getUri(), 1);

        return $this->redirect($url, 301);
    }
}
